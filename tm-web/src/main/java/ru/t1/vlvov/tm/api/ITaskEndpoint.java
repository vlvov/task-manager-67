package ru.t1.vlvov.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.vlvov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/save")
    Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

    @WebMethod
    @GetMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

    @WebMethod
    @GetMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

}
