package ru.t1.vlvov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBackupSaveRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class DataBackupSaveListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save backup data to base 64 file.";

    @NotNull
    public static final String NAME = "data-save-backup";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBackupSaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull DataBackupSaveRequest request = new DataBackupSaveRequest(getToken());
        domainEndpoint.saveDataBackup(request);
    }

}
