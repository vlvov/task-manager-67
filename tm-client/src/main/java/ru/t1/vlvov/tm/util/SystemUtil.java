package ru.t1.vlvov.tm.util;

import org.jetbrains.annotations.Nullable;

import java.lang.management.ManagementFactory;

public interface SystemUtil {

    static long getPID() {
        @Nullable final String processName = ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            return Long.parseLong(processName.split("@")[0]);
        }
        return 0;
    }

}
